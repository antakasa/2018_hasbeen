import React from "react";
import Menu from "../../components/menu/index.js";
import Footer from "../../components/footer/index.js";
import Header from "../../components/hero/header.js";
import Hero from "../../components/hero/index.js";
import Logo from "../../components/logo/index.js";
import SubHeader from "../../components/subHeader/index.js";
import EpisodeGrid from "../../components/EpisodesMusicGrid/index.js";
import ContentContainer from "../../components/ContentContainer/index.js";
import HeroIMG from "../../imgs/vlogit.jpg";
export default class Episodes extends React.Component {
  render() {
    const { kicker, data } = this.props;
    return (
      <div>
        <Hero heroIMG={HeroIMG}>
          <Logo topLeft />
        </Hero>
        <Header header={"Dallas Vlod"} description={kicker} />
        <ContentContainer id="ContentContainer_frontPage">
          <EpisodeGrid data={data} contentType="vlog" />
          <div style={{ height: "20px" }} />
        </ContentContainer>
        <Footer />
      </div>
    );
  }
}
