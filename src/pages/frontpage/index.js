import React from "react";
import Menu from "../../components/menu/index.js";
import Footer from "../../components/footer/index.js";
import Header from "../../components/hero/header.js";
import Hero from "../../components/hero/index.js";
import Logo from "../../components/logo/index.js";
import { withWindowSizeListener } from "react-window-size-listener";
import Label from "../../components/label/index.js";
import { ShowMoreButton } from "../../components/buttons/index.js";
import GridOrSwiper from "../../components/gridOrSwiper/index.js";
import LazyLoad from "react-lazy-load";
import ContentContainer from "../../components/ContentContainer/index.js";
import { images, profileData, createRandomArray } from "../../helpers/index.js";
import SubHeader from "../../components/subHeader/index.js";
import Card from "../../components/Card/index.js";
import HeroIMG from "../../imgs/etusivu.jpg";
import Music from "../../components/music/index.js";
import EpisodePreview from "../../components/episodePreview/index.js";
import TextContent from "../../textContent.js";
import GlitchText from "../../components/GlitchText";
import skull from "../../components/hero/skull.svg";
import cloud from "../../components/hero/storm_cloud.svg";
import microphone from "../../components/hero/microphone.svg";
import AreenaClip from "../../components/areenaClip";
//<SubHeader link={"/jaksot"}>Jaksot</SubHeader>

const CreateHeader = ({ text, animations, additionalStyles }) => (
  <div
    style={{
      fontFamily: "Bungee",
      position: "relative",
      zIndex: 2,
      fontSize: "40px",
      lineHeight: "1",
      marginLeft: "15px",
      color: "white",
      marginBottom: "15px",
      ...additionalStyles
    }}
  >
    {animations && (
      <GlitchText text={text} images={[skull, cloud, microphone]} />
    )}
  </div>
);

class FrontPage extends React.Component {
  state = {
    animations: true
  };

  render() {
    return (
      <div>
        <Hero
          height={this.props.windowSize.windowWidth > 800 ? "80vh" : null}
          heroIMG={HeroIMG}
        >
          <Logo center />
        </Hero>
        <ContentContainer id="ContentContainer_frontPage">
          <CreateHeader
            text="Jaksot"
            animations={this.state.animations}
            additionalStyles={{}}
          />
          <LazyLoad>
            <EpisodePreview width={this.props.windowSize.windowWidth} />
          </LazyLoad>
          <ShowMoreButton newPath="/jaksot">Kaikki jaksot</ShowMoreButton>
          <CreateHeader
            text="Tarinat"
            animations={this.state.animations}
            additionalStyles={{ marginTop: "90px", marginBottom: "-15px" }}
          />
          <Label
            nick={profileData().name("niklas")}
            handle={profileData().nick("niklas")}
          />
          <GridOrSwiper
            toggleAnimation={() =>
              this.setState({ animations: !this.state.animations })
            }
            filters={["niklaksenFeedi"]}
            numberOfItemsInGrid={6}
          >
            <Card images={images} />
          </GridOrSwiper>
          <Label
            nick={profileData().name("salla")}
            handle={profileData().nick("salla")}
          />
          <GridOrSwiper
            toggleAnimation={() =>
              this.setState({ animations: !this.state.animations })
            }
            filters={["sallanFeedi"]}
            numberOfItemsInGrid={6}
          >
            <Card images={images} />
          </GridOrSwiper>

          <ShowMoreButton newPath="/tarinat">Kaikki tarinat</ShowMoreButton>

          <CreateHeader
            text="Musiikki"
            disabled={this.state.disabled}
            additionalStyles={{ marginTop: "90px", marginBottom: "30px" }}
            animations={this.state.animations}
          />
          <LazyLoad>
            <Music data={TextContent.musiikki} />
          </LazyLoad>
        </ContentContainer>

        <LazyLoad>
          <Footer />
        </LazyLoad>
      </div>
    );
  }
}

export default withWindowSizeListener(FrontPage);
