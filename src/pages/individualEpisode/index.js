import React from "react";
import styled from "styled-components";
import Footer from "../../components/footer/episodeFooter.js";
import Header from "../../components/hero/header.js";
import Hero from "../../components/hero/index.js";
import Logo from "../../components/logo/index.js";
import Head from "./head.js";
import SubHeader from "../../components/subHeader/index.js";
import GridOrSwiper from "../../components/gridOrSwiper/index.js";
import ContentContainer from "../../components/ContentContainer/index.js";
import { ShowMoreButton } from "../../components/buttons/index.js";
import { images } from "../../helpers/index.js";
import HeroIMG from "../../imgs/jakso_01.jpg";
import Card from "../../components/Card/index.js";
import Music from "../../components/music/index.js";
import TextContent from "../../textContent.js";
import { withWindowSizeListener } from "react-window-size-listener";
import EpisodePreview from "../../components/episodePreview/index.js";
import Data from "../../textContent.js";
import AreenaClip from "../../components/areenaClip/index.js";
const DetermineId = epNum => Data.jaksot[epNum - 1].areenaID;
class IndividualStory extends React.Component {
  render() {
    const { jaksonNimi, tulossa, pituus, kuvaus, areenaID } = this.props.data;
    return (
      <div>
        <Hero heroIMG={HeroIMG}>
          <Logo topLeft />
        </Hero>

        <Head
          areenaId={areenaID}
          epName={jaksonNimi}
          epComing={tulossa}
          epDesc={kuvaus}
          epNum={this.props.episodeNumber}
          epLength={pituus}
        />
        <ContentContainer id="ContentContainer_ind_story">
          <div
            style={{
              marginTop: "30px",
              marginBottom: "30px",
              marginLeft: "15px",
              width: "calc(100% - 30px)"
            }}
          >
            <AreenaClip id={DetermineId(this.props.episodeNumber)} />
          </div>
          <SubHeader
            link={"/tarinat"}
            additionalStyles={{
              marginBottom: "30px",
              marginTop: "90px"
            }}
          >
            Jakson tarinat
          </SubHeader>
          <GridOrSwiper
            filters={this.props.episodeNumber}
            numberOfItemsInGrid={6}
          >
            <Card images={images} />
          </GridOrSwiper>
          <ShowMoreButton newPath="/tarinat">
            Näytä kaikki tarinat
          </ShowMoreButton>

          <SubHeader
            additionalStyles={{
              marginBottom: "30px",
              marginTop: "90px"
            }}
          >
            Jakson musat
          </SubHeader>
          <Music
            data={TextContent.jaksokohtaisetMusat[this.props.episodeNumber]}
          >
            {" "}
          </Music>

          <ShowMoreButton
            newPath="/musa"
            additionalStyles={{
              marginTop: "-60px",
              marginBottom: "90px"
            }}
          >
            Näytä kaikki biisit
          </ShowMoreButton>
        </ContentContainer>

        <Footer />
      </div>
    );
  }
}

export default IndividualStory;
