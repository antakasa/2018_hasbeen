import React from "react";
import styled from "styled-components";
import { WatchEpisodeButton } from "../../components/buttons/index.js";
import { leftMarginCalculator } from "../../helpers/index.js";

import { withWindowSizeListener } from "react-window-size-listener";
const Container = styled.div`
  margin-left: 29px;
  max-width: 450px;
  font-family: "Open Sans";
  display: flex;
  flex-direction: column;
  margin-left: ${props => leftMarginCalculator(props.width)};
  margin-right: 15px;
`;

const MetaData = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  font-size: 13px;
  line-height: 1.35;
  letter-spacing: normal;
  text-align: left;
  color: #df5d5d;
`;

const EpisodeNumber = styled.div`
  width: 30%;
`;

const EpisodeDuration = styled.div`
  width: 70%;
`;

const EpisodeName = styled.div`
  width: 100%;
  font-family: Bungee;
  font-size: 40px;
  line-height: 1.4;
  letter-spacing: normal;
  text-align: left;
  color: #ffffff;
`;

const Coming = styled.div`
  width: 100%;
  font-size: 20px;
  line-height: 1.6;
  letter-spacing: normal;
  text-align: left;
  color: #df5d5d;
`;

const Description = styled.div`
  width: 100%;
  font-size: 13px;
  line-height: 1.6;
  letter-spacing: normal;
  text-align: left;
  color: #ffffff;
`;

const WatchEpisode = styled.div`
  width: 100%;
`;

const Head = ({
  epNum,
  epComing,
  epName,
  epLength,
  windowSize,
  epDesc,
  areenaID
}) => (
  <Container width={windowSize.windowWidth}>
    <MetaData>
      <EpisodeNumber>Jakso {epNum}</EpisodeNumber>
      <EpisodeDuration>{epLength}</EpisodeDuration>
    </MetaData>
    <EpisodeName>{epName}</EpisodeName>
    <Description>{epDesc}</Description>
    {epComing && <Coming>Tulossa {epComing}</Coming>}
  </Container>
);

export default withWindowSizeListener(Head);
