import React from "react";
import styled from "styled-components";
import Footer from "../../components/footer/";
import Header from "../../components/hero/header.js";
import Hero from "../../components/hero/index.js";
import Logo from "../../components/logo/index.js";
import Head from "./head.js";
import SubHeader from "../../components/subHeader/index.js";
import GridOrSwiper from "../../components/gridOrSwiper/index.js";
import ContentContainer from "../../components/ContentContainer/index.js";
import { ShowMoreButton } from "../../components/buttons/index.js";
import { images } from "../../helpers/index.js";
import HeroIMG from "../../imgs/vlogit.jpg";
import Card from "../../components/Card/index.js";
import Music from "../../components/music/index.js";
import TextContent from "../../textContent.js";
import { withWindowSizeListener } from "react-window-size-listener";
import EpisodePreview from "../../components/episodePreview/index.js";
import Youtube from "../../components/youtubeClip";

import Data from "../../textContent.js";
const DetermineId = epNum => Data.jaksot[epNum - 1].areenaID;
class IndividualStory extends React.Component {
  render() {
    const { jaksonNimi, tulossa, pituus, kuvaus, YoutubeID } = this.props.data;
    return (
      <div>
        <Hero heroIMG={HeroIMG}>
          <Logo topLeft />
        </Hero>

        <Head
          epName={jaksonNimi}
          epComing={tulossa}
          epDesc={kuvaus}
          epNum={this.props.episodeNumber}
          epLength={pituus}
        />
        <ContentContainer id="ContentContainer_ind_story">
          <div
            style={{
              marginTop: "30px",
              marginBottom: "30px",
              marginLeft: "15px",
              width: "calc(100% - 30px)"
            }}
          >
            <Youtube
              id={YoutubeID}
              width={
                this.props.windowSize.windowWidth - 30 > 450
                  ? 450 - 30
                  : this.props.windowSize.windowWidth - 30
              }
            />
          </div>
        </ContentContainer>

        <ShowMoreButton newPath="/vlogit" additionalStyles={{}}>
          Näytä kaikki osat
        </ShowMoreButton>
        <Footer />
      </div>
    );
  }
}

export default withWindowSizeListener(IndividualStory);
