import React from "react";
import Menu from "../../components/menu/index.js";
import Footer from "../../components/footer/index.js";
import Header from "../../components/hero/header.js";
import Hero from "../../components/hero/index.js";
import Logo from "../../components/logo/index.js";
import { withWindowSizeListener } from "react-window-size-listener";
import LazyLoad from "react-lazy-load";
import ContentContainer from "../../components/ContentContainer/index.js";
import FilterList from "../../components/filterList/index.js";
import HeroIMG from "../../imgs/tarinat.jpg";
class FrontPage extends React.Component {
  render() {
    return (
      <div style={{ minHeight: "200vh" }}>
        <Hero heroIMG={HeroIMG}>
          <Logo topLeft />
        </Hero>
        <Logo topLeft />
        <Header header={"Tarinat"} description={this.props.kicker} />
        <ContentContainer id="ContentContainer_stories">
          <FilterList width={this.props.windowSize.windowWidth} />
        </ContentContainer>
        <Footer />
      </div>
    );
  }
}

export default withWindowSizeListener(FrontPage);
