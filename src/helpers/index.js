import WebFont from "webfontloader";
import Data from "../data_uusi.json";
import NiklasIMG from "../components/label/imgs/niklas.png";
import SallaIMG from "../components/label/imgs/salla.png";
import moment from "moment";

function ResizeSensor(element, callback) {
  console.log(element);
  let zIndex = parseInt(getComputedStyle(element));
  if (isNaN(zIndex)) {
    zIndex = 0;
  }
  zIndex--;

  let expand = document.createElement("div");
  expand.style.position = "absolute";
  expand.style.left = "0px";
  expand.style.top = "0px";
  expand.style.right = "0px";
  expand.style.bottom = "0px";
  expand.style.overflow = "hidden";
  expand.style.zIndex = zIndex;
  expand.style.visibility = "hidden";

  let expandChild = document.createElement("div");
  expandChild.style.position = "absolute";
  expandChild.style.left = "0px";
  expandChild.style.top = "0px";
  expandChild.style.width = "10000000px";
  expandChild.style.height = "10000000px";
  expand.appendChild(expandChild);

  let shrink = document.createElement("div");
  shrink.style.position = "absolute";
  shrink.style.left = "0px";
  shrink.style.top = "0px";
  shrink.style.right = "0px";
  shrink.style.bottom = "0px";
  shrink.style.overflow = "hidden";
  shrink.style.zIndex = zIndex;
  shrink.style.visibility = "hidden";

  let shrinkChild = document.createElement("div");
  shrinkChild.style.position = "absolute";
  shrinkChild.style.left = "0px";
  shrinkChild.style.top = "0px";
  shrinkChild.style.width = "200%";
  shrinkChild.style.height = "200%";
  shrink.appendChild(shrinkChild);

  element.appendChild(expand);
  element.appendChild(shrink);

  function setScroll() {
    expand.scrollLeft = 10000000;
    expand.scrollTop = 10000000;

    shrink.scrollLeft = 10000000;
    shrink.scrollTop = 10000000;
  }
  setScroll();

  let size = element.getBoundingClientRect();

  let currentWidth = size.width;
  let currentHeight = size.height;

  let onScroll = function() {
    let size = element.getBoundingClientRect();

    let newWidth = size.width;
    let newHeight = size.height;

    if (newWidth != currentWidth || newHeight != currentHeight) {
      currentWidth = newWidth;
      currentHeight = newHeight;

      callback();
    }

    setScroll();
  };

  expand.addEventListener("scroll", onScroll);
  shrink.addEventListener("scroll", onScroll);
}

const range = (min, max) => ({ min, max });

const loadFont = (arr, cb) => {
  WebFont.load({
    google: {
      families: arr
    },
    fontactive: () => cb()
  });
};

const sessionGetSets = {
  get: () =>
    typeof sessionStorage.HasBeen !== "undefined"
      ? JSON.parse(sessionStorage.HasBeen)
      : 0,
  set: value => (sessionStorage.HasBeen = JSON.stringify(value))
};

const sortToAscendingOrder = (a, b) => {
  const accordingTo = "tiedostonNimi";

  const A = addYearDeleteChars(a[accordingTo]);
  const B = addYearDeleteChars(b[accordingTo]);

  let comparison = 0;
  if (A > B) {
    comparison = 1;
  } else if (A < B) {
    comparison = -1;
  }
  return comparison;
};

const profileData = () => {
  const is = e => e && e.toUpperCase() === "NIKLAS";
  return {
    image: who => (is(who) ? NiklasIMG : SallaIMG),
    name: who => (is(who) ? "Niklas" : "Salla"),
    nick: who => (is(who) ? "nicokiszero" : "musicaecompendium")
  };
};

const checkWhoseStory = id => {
  const rightObject = Data.filter(el => el.tiedostonNimi === id);
  if (rightObject.length !== 1) {
    return;
  }

  if (rightObject[0].hasOwnProperty("niklaksenFeedi")) {
    return "niklas";
  } else if (rightObject[0].hasOwnProperty("sallanFeedi")) {
    return "salla";
  }
};

const getRandomElementsFromArray = (
  array,
  numberOfRandomElementsToExtract = 1
) => {
  const elements = [];

  const getRandomElement = arr => {
    if (elements.length < numberOfRandomElementsToExtract) {
      const index = Math.floor(Math.random() * arr.length);
      const element = arr.splice(index, 1)[0];

      elements.push(element);

      return getRandomElement(arr);
    } else {
      return elements;
    }
  };

  return getRandomElement([...array]);
};

const extendStringObject = () => {
  String.prototype.insertAt = function(index, string) {
    return this.substr(0, index) + string + this.substr(index);
  };
};

const addYearDeleteChars = mmdd => mmdd.insertAt(0, "2018").replace(/\D/g, "");

const checkTimeLimit = (e, timeProperty, timeLimit) => {
  if (!timeProperty) timeProperty = "tiedostonNimi";
  // CHECKTIMELIMIT IS USED TO EXCLUDE CONTENT THAT SHOULD NOT BE PUBLISHED YET
  const mmdd =
    typeof e[timeProperty] !== "string"
      ? JSON.stringify(e[timeProperty])
      : e[timeProperty];

  const yearAdded = addYearDeleteChars(mmdd);
  return yearAdded > timeLimit;
};

const timeLimitStore = () => "20181018";

const checkUndef = e => typeof e === "undefined";

const filter = (filters, timeProperty, timeLimit) => {
  const filtered = Data.filter(function(el) {
    if (timeLimit && checkTimeLimit(el, timeProperty, timeLimit)) return false;
    let boolean = true;
    for (let i = 0; i < filters.length; i++) {
      if (checkUndef(el[filters[i]])) {
        boolean = false;
        break;
      }
    }
    return boolean;
  });
  return filtered;
};

function importAll(r) {
  let images = {};
  r.keys().map((item, index) => {
    images[item.replace("./", "").replace(".jpg", "")] = r(item);
  });
  return images;
}

const filterBasedOnEpisodeNumber = epNum => {
  const filtered = Data.filter(el => {
    return epNum === el["jaksonumero(t)"];
  });
  return filtered;
};

const createRandomArray = length =>
  Array.from({ length: length }, () => Math.floor(Math.random() * 10));

const images = importAll(
  require.context("../imgs/thumbs", false, /\.(png|jpe?g|svg)$/)
);

export const vlogImages = () => {
  const object = importAll(
    require.context("../imgs/vlogit", false, /\.(png|jpe?g|svg)$/)
  );

  let array = [];
  for (var key in object) {
    if (object.hasOwnProperty(key)) {
      array.push(object[key]);
    }
  }
  return array;
};

const episodeImages = () => {
  const object = importAll(
    require.context("../imgs/jaksot", false, /\.(png|jpe?g|svg)$/)
  );

  let array = [];
  for (var key in object) {
    if (object.hasOwnProperty(key)) {
      array.push(object[key]);
    }
  }
  return array;
};

const leftMarginCalculator = width => {
  if (width > 860) {
    return "calc(25% + 15px)";
  } else if (width > 500 && width < 860) {
    return "10%";
  } else {
    return "15px";
  }
};

export {
  extendStringObject,
  range,
  loadFont,
  filter,
  images,
  checkWhoseStory,
  profileData,
  sortToAscendingOrder,
  timeLimitStore,
  filterBasedOnEpisodeNumber,
  createRandomArray,
  sessionGetSets,
  episodeImages,
  importAll,
  ResizeSensor,
  getRandomElementsFromArray,
  leftMarginCalculator
};
