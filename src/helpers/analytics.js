import { sessionGetSets } from "./index.js";
const checkAnalytics = () =>
  typeof window.yleAnalytics !== "undefined" &&
  typeof window.yleAnalytics.trackEvent !== "undefined"
    ? window.yleAnalytics.trackEvent
    : (a, b) => console.log(`${a} rekisteröity offline`);

const whichMilestone = number => {
  switch (true) {
    case number > 3 && number <= 10:
      return "3-10";
    case number >= 10 && number <= 20:
      return "10-20";
    case number > 20:
      return "yli 20";
    default:
      return "alle 3";
  }
};

const Analytics = (() => {
  return {
    registerStoryStart: () => {
      const trackEvent = checkAnalytics();
      trackEvent(`hasbeen.stories.storyComponentStarts`, {
        pageName: "HasBeen"
      });
    },
    registerEvent: name => {
      const trackEvent = checkAnalytics();
      trackEvent(`hasbeen.${name}`, {
        pageName: "HasBeen"
      });
    },
    getFromStorage: () => (sessionGetSets.get() ? sessionGetSets.get() : 0),

    trackStoryMilestone: newRound => {
      const trackEvent = checkAnalytics();
      trackEvent(`hasbeen.storyMilestone.${whichMilestone(newRound)}`, {
        pageName: "HasBeen"
      });
    }
  };
})();

export default Analytics;
