export default {
  sivut: {
    tarinat: "Käy tarinoiden kimppuun, näitä et enää Instagramista näe!",
    jaksot: "20-vuotias ja hasbeen − onko Niklaksella enää toivoa?",
    musiikki: "Biisit, jotka Niklas haluaisi jo unohtaa ja uudempaa kamaa.",
    vlog: "Mitä Sallalle kuuluu nyt? #DallasVlod & @musicaecompendium"
  },
  musiikki: [
    {
      esittaja: "MakeXMikkiH",
      kappale: "Fiilaan sun moovei",
      spotifyURI: "spotify:track:2UJzCxcQOgCI5T4sV1Cgyt"
    },
    {
      esittaja: "Salla",
      kappale: "All On My Own",
      spotifyURI: "spotify:track:6KIqM1g8Jtqc8kWqTY2xeJ"
    },
    {
      esittaja: "NIC",
      kappale: "Temput ja biitit",
      spotifyURI: "spotify:track:6gpf0J2xC7hpVmbufsfX6f"
    },
    {
      esittaja: "NIC",
      kappale: "Oo oma ittes",
      spotifyURI: "spotify:track:359UOgD4Pa4O27FDzZC7bW"
    },
    {
      esittaja: "RENÉ",
      kappale: "Täytyy lähteä",
      spotifyURI: "spotify:track:7GfuO9wNd9Z36xx7GC6j0H"
    },
    {
      esittaja: "RENÉ",
      kappale: "Huomaamaton",
      spotifyURI: "spotify:track:1Dqwzzvn5F5OOgboDxBt3q"
    },
    {
      esittaja: "RENÉ",
      Kkappale: "Vapaa",
      spotifyURI: "spotify:track:44O6Aq1OGjVyBVZabmOarW"
    },
    {
      esittaja: "NIC",
      kappale: "Hiki",
      spotifyURI: "spotify:track:1MUCL0483mPFDdsu9e6JiW"
    }
  ],
  vlogit: [
    {
      jaksonNimi: "Ykköstykki ja Prinsessa Ruusunen",
      kuvaus:
        "Content warning: nuuskan väärinkäyttöä, viewer discretion is advised.",
      pituus: "6 min",
      YoutubeID: "C2T-dVTM-jQ"
    },
    {
      jaksonNimi: "Once More With Feeling",
      kuvaus: "Kaunista musiikkia ja tulevaisuuden planeja!",
      pituus: "6 min",
      YoutubeID: "Nha0hpP_DHk"
    }
  ],
  jaksot: [
    {
      jaksonNimi: "Niklas",
      kuvaus:
        "Niklas saa roolin Uudesta Päivästä, mutta freelancerin arki yllättää.",
      pituus: "10 min",
      areenaID: "1-4216771"
    },
    {
      jaksonNimi: "Salla",
      kuvaus:
        "Salla miettii haluaako tehdä musiikkia enää lainkaan, ja jos, niin kenen kanssa.",
      pituus: "11 min",
      areenaID: "1-4216773"
    },
    {
      jaksonNimi: "Buli ilta",
      kuvaus:
        "Tärkeä gaala uhkaa jäädä kun Niklas ja Salla pistävät kaupungin sileäksi.",
      pituus: "13 min",
      areenaID: "1-4216774"
    },
    {
      jaksonNimi: "Duo",
      kuvaus:
        "Salla elää biisinikkarin arkea. Niklaskin joutuu kohtaamaan todellisuuden.",
      pituus: "14 min",
      areenaID: "1-4216775"
    },
    {
      jaksonNimi: "NIC",
      kuvaus:
        "Rahan perässä tehty visiitti vie Niklaksen aikamatkalle kuuden vuoden taakse.",
      pituus: "10 min",
      areenaID: "1-4216776"
    },
    {
      jaksonNimi: "Levy-yhtiö",
      kuvaus: "Pauliina haluaa kuulla Niklaksen ja Sallan biisejä.",
      pituus: "14 min",
      areenaID: "1-4216777"
    },
    {
      jaksonNimi: "Showcase",
      kuvaus: "Niklas ja Salla vetävät keikan mielipidevaikuttajille.",
      pituus: "11 min",
      areenaID: "1-4216778"
    },
    {
      jaksonNimi: "Hiki",
      kuvaus: "Teinitähti NICille puuhataan toista levyä.",
      pituus: "13 min",
      areenaID: "1-4216779"
    },
    {
      jaksonNimi: "Radiosoitto",
      kuvaus: "Salla tekee biisit, Niklas tekee promot.",
      pituus: "13 min",
      areenaID: "1-4216780"
    },
    {
      jaksonNimi: "Diili",
      kuvaus: "Pöydällä on sopimus - millainen, ja kenelle?",
      pituus: "14 min",
      areenaID: "1-4216781"
    }
  ],
  jaksokohtaisetMusat: {
    1: [
      {
        spotifyURI: "spotify:track:2UJzCxcQOgCI5T4sV1Cgyt",
        Esittaja: "MakeXMikkiH",
        Kappale: "Fiilaan sun moovei"
      },
      {
        spotifyURI: "spotify:track:6gpf0J2xC7hpVmbufsfX6f",
        Esittaja: "NIC",
        Kappale: "Temput ja biitit"
      }
    ],
    2: [
      {
        spotifyURI: "spotify:track:6KIqM1g8Jtqc8kWqTY2xeJ",
        Esittaja: "Salla",
        Kappale: "All On My Own"
      },
      {
        spotifyURI: "spotify:track:2UJzCxcQOgCI5T4sV1Cgyt",
        Esittaja: "MakeXMikkiH",
        Kappale: "Fiilaan sun moovei"
      },
      {
        spotifyURI: "spotify:track:6gpf0J2xC7hpVmbufsfX6f",
        Esittaja: "NIC",
        Kappale: "Temput ja biitit"
      }
    ],
    3: [
      {
        spotifyURI: "spotify:track:0fHVm9sfZQ5FlAvEtVmeyN",
        Esittaja: "SANNI",
        Kappale: "Että mitähän vittua"
      }
    ],
    4: [
      {
        spotifyURI: "spotify:track:6KIqM1g8Jtqc8kWqTY2xeJ",
        Esittaja: "Salla",
        Kappale: "All On My Own"
      },
      {
        spotifyURI: "spotify:track:2UJzCxcQOgCI5T4sV1Cgyt",
        Esittaja: "MakeXMikkiH",
        Kappale: "Fiilaan sun moovei"
      }
    ],
    5: [
      {
        spotifyURI: "spotify:track:359UOgD4Pa4O27FDzZC7bW",
        Esittaja: "NIC",
        Kappale: "Oo oma ittes"
      },
      {
        spotifyURI: "spotify:track:6gpf0J2xC7hpVmbufsfX6f",
        Esittaja: "NIC",
        Kappale: "Temput ja biitit"
      },
      {
        spotifyURI: "spotify:track:2UJzCxcQOgCI5T4sV1Cgyt",
        Esittaja: "MakeXMikkiH",
        Kappale: "Fiilaan sun moovei"
      }
    ],
    6: [
      {
        spotifyURI: "spotify:track:7GfuO9wNd9Z36xx7GC6j0H",
        Esittaja: "RENÉ",
        Kappale: "Täytyy lähteä"
      },
      {
        spotifyURI: "spotify:track:1Dqwzzvn5F5OOgboDxBt3q",
        Esittaja: "RENÉ",
        Kappale: "Huomaamaton"
      }
    ],
    7: [
      {
        spotifyURI: "spotify:track:44O6Aq1OGjVyBVZabmOarW",
        Esittaja: "RENÉ",
        Kappale: "Vapaa"
      },
      {
        spotifyURI: "spotify:track:1Dqwzzvn5F5OOgboDxBt3q",
        Esittaja: "RENÉ",
        Kappale: "Huomaamaton"
      },
      {
        spotifyURI: "spotify:track:7GfuO9wNd9Z36xx7GC6j0H",
        Esittaja: "RENÉ",
        Kappale: "Täytyy lähteä"
      }
    ],
    8: [
      {
        spotifyURI: "spotify:track:1MUCL0483mPFDdsu9e6JiW",
        Esittaja: "NIC",
        Kappale: "Hiki"
      }
    ],
    9: [
      {
        spotifyURI: "spotify:track:7GfuO9wNd9Z36xx7GC6j0H",
        Esittaja: "RENÉ",
        Kappale: "Täytyy lähteä"
      }
    ],
    10: [
      {
        spotifyURI: "spotify:track:1kr6SCgL0eSywWX0ewbMGz",
        Esittaja: "Scandinavian Music Group",
        Kappale: "Baabel"
      }
    ]
  }
};
