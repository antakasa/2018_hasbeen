import React from "react";
import styled from "styled-components";
import { WatchEpisodeButton } from "../../components/buttons/index.js";
import { leftMarginCalculator } from "../../helpers/index.js";

import { withWindowSizeListener } from "react-window-size-listener";
const Container = styled.div`
  margin-left: 30px;
  width: 100%;
  max-width: 450px;
  font-family: "Open Sans";
  display: flex;
  flex-direction: column;
`;

const MetaData = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  font-size: ${props => (props.width < 600 ? "13px" : "18px")};
  line-height: 1.35;
  letter-spacing: normal;
  text-align: left;
  color: #df5d5d;
`;

const EpisodeNumber = styled.div`
  width: 40%;
`;

const EpisodeDuration = styled.div`
  width: 60%;
`;

const EpisodeName = styled.div`
  width: 100%;
  font-family: Bungee;
  font-size: ${props => (props.width < 600 ? "15px" : "20px")};
  line-height: 1.4;
  letter-spacing: normal;
  text-align: left;
  color: #ffffff;
`;

const Description = styled.div`
  width: 100%;
  font-size: ${props => (props.width < 600 ? "10px" : "17px")};
  line-height: 1.6;
  letter-spacing: normal;
  text-align: left;
  color: #ffffff;
`;

const WatchEpisode = styled.div`
  width: 100%;
`;

const EpisodeDetails = ({
  epNum,
  epLength,
  epName,
  epDesc,
  windowSize,
  vlog
}) => (
  <Container width={windowSize.windowWidth}>
    <MetaData width={windowSize.windowWidth}>
      <EpisodeNumber>
        {vlog ? "Osa" : "Jakso"} {epNum}
      </EpisodeNumber>
      <EpisodeDuration>{epLength} </EpisodeDuration>
    </MetaData>
    <EpisodeName width={windowSize.windowWidth}>{epName}</EpisodeName>
    <Description width={windowSize.windowWidth}>{epDesc}</Description>
  </Container>
);

export default withWindowSizeListener(EpisodeDetails);
