import React from "react";
import styled from "styled-components";
import AreenaClip from "../areenaClip/index.js";
import SpotifyPlayer from "react-spotify-player";
import { withWindowSizeListener } from "react-window-size-listener";
import { createRandomArray } from "../../helpers/index.js";
import { episodeImages, vlogImages } from "../../helpers/index.js";
import EpisodeDetails from "./episodeDetails.js";
import { HashRouter as Router, Link } from "react-router-dom";
const calculateHeight = fullScreenWidth => {
  if (fullScreenWidth < 900) {
    return 280;
  }

  let contentWidth = fullScreenWidth / 2;
  let margin = 20;
  let playerWidth = contentWidth / 2 - margin;
  let playerHeight = playerWidth + 0.22 * playerWidth;
  return playerHeight;
};

let margin = 20;

const calculateWidth = fullScreenWidth => {
  if (fullScreenWidth < 900) {
    return 210;
  }
  let contentWidth = fullScreenWidth / 2;
  let playerWidth = contentWidth / 2 - margin;
  return playerWidth;
};

const EpisodeImage = styled.img`
  width: 40%;
  height: 40%;
  align-self: center;
`;

const view = "coverart"; // or 'coverart'
const theme = "black"; // or 'white'
const images = episodeImages();
const vlogImgs = vlogImages();
const GridContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const Item = styled.div`
  display: flex;
  flex-direction: row;
  margin: 10px;
`;

const SongDetails = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  margin-top: 20%;
  margin-left: ${props => (props.width > 900 ? "50px" : "8px")};
`;

const SongName = styled.p`
  margin-top: 0;
  text-transform: uppercase;
  font-family: "Open Sans";
  font-size: 16px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.75;
  letter-spacing: normal;
  text-align: left;
  color: #ffffff;
  margin-bottom: 0;
`;

const SongArtist = styled.p`
  font-family: "Open Sans";
  font-size: 14px;
  margin-top: 0;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.5;
  letter-spacing: normal;
  text-align: left;
  color: #df5d5d;
`;

class EpisodeItem extends React.Component {
  leftColumnCreator = (width, i, src) => {
    const size = {
      width: calculateWidth(width),
      height: calculateHeight(width)
    };
    if (
      this.props.contentType === "video" ||
      this.props.contentType === "vlog"
    ) {
      return <EpisodeImage src={src} />;
    } else if (this.props.contentType === "spotify") {
      return <SpotifyPlayer uri={src} size={size} view={view} theme={theme} />;
    } else {
      return null;
    }
  };
  render() {
    let { data } = this.props;

    return (
      <GridContainer>
        {this.props.contentType === "video" && (
          <Router>
            <React.Fragment>
              {data.map((e, i) => (
                <Link to={e.tulossa ? "/jaksot" : `/jaksot/${i + 1}`}>
                  <Item key={i}>
                    {this.leftColumnCreator(
                      this.props.windowSize.windowWidth,
                      i,
                      images[i]
                    )}
                    {this.props.contentType === "video" && (
                      <EpisodeDetails
                        epName={e.jaksonNimi}
                        epDesc={e.tulossa ? `Tulossa ${e.tulossa}` : e.kuvaus}
                        epNum={i + 1}
                        epLength={e.pituus}
                      />
                    )}
                  </Item>
                </Link>
              ))}
            </React.Fragment>
          </Router>
        )}
        {this.props.contentType === "spotify" && (
          <React.Fragment>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center"
              }}
            >
              {data.map((e, i) => {
                if (data[i].spotifyURI.length < 2) return null;
                return (
                  <Item key={i}>
                    {this.leftColumnCreator(
                      this.props.windowSize.windowWidth,
                      i,
                      data[i].spotifyURI
                    )}
                  </Item>
                );
              })};
            </div>
          </React.Fragment>
        )}
        {this.props.contentType === "vlog" && (
          <Router>
            <React.Fragment>
              {data.map((e, i) => (
                <Link to={e.tulossa ? "/vlogit" : `/vlogit/${i + 1}`}>
                  <Item key={i}>
                    {this.leftColumnCreator(
                      this.props.windowSize.windowWidth,
                      i,
                      vlogImgs[i]
                    )}
                    {this.props.contentType === "vlog" && (
                      <EpisodeDetails
                        epName={e.jaksonNimi}
                        epDesc={e.tulossa ? `Tulossa ${e.tulossa}` : e.kuvaus}
                        epNum={i + 1}
                        epLength={e.pituus}
                        vlog
                      />
                    )}
                  </Item>
                </Link>
              ))}
            </React.Fragment>
          </Router>
        )}
      </GridContainer>
    );
  }
}

export default withWindowSizeListener(EpisodeItem);
