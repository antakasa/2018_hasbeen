import React from "react";
import Data from "../../data_uusi.json";
import Grid from "../grid/index.js";
import { TagButton } from "../buttons/index.js";
import Stories from "../stories/App.js";
import LazyLoad from "react-lazy-load";
import "./lazy.css";
import { filter } from "../../helpers/index.js";
import styled, { keyframes } from "styled-components";
import { images } from "../../helpers/index.js";
import Card from "../Card/index.js";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import { timeLimitStore, sortToAscendingOrder } from "../../helpers/index.js";
import { withWindowSizeListener } from "react-window-size-listener";
const ImageContainer = styled.div`
  flex-grow: 1;
  box-sizing: border-box;
  background: rgba(0, 0, 0, 0);
  color: #171e42;
  padding: 0px;
  margin-left: ${props => (props.width < 920 ? "10px" : "40px")};
  margin-top: ${props => (props.width < 920 ? "10px" : "40px")};
  max-width: ${props =>
    props.width < 920 ? "calc(50% - 10px)" : "calc(33.33333% - 40px)"};
`;

const TagContainer = styled.div`
  width: ${props => (props.width < 600 ? "100%" : "100%")};
  margin-left: 0px;
  margin-bottom: 40px;
  transition: all 300ms ease-in-out;
`;

class Filterlist extends React.Component {
  state = {
    filters: ["niklas&Lise"],
    currentVideo: 0,
    modalOpen: false
  };

  componentWillMount() {
    this.filter = filter.bind(this);
    this.storeFilteredVideos(["niklas&Lise"]);
  }

  getTags = () => {
    return [
      { name: "Kaikki", id: "tiedostonNimi" },
      { name: "Niklas", id: "niklas" },
      { name: "Niklaksen feedi", id: "niklaksenFeedi" },
      { name: "Salla", id: "salla" },
      { name: "Sallan feedi", id: "sallanFeedi" },
      { name: "Salla & Niklas", id: "salla&Niklas" },
      { name: "Niklas & Lise", id: "niklas&Lise" },
      { name: "Sallan studio", id: "sallanTyöhuone" },
      { name: "Bileet", id: "bileet" },
      { name: "Starat", id: "starat" },
      { name: "Festarit", id: "festarit" }
    ];
  };

  storeFilteredVideos = filters =>
    this.setState({
      videoList: this.filter(filters, null, timeLimitStore())
        .sort(sortToAscendingOrder)
        .map(e => e.tiedostonNimi)
    });

  changeFilters = filter => {
    if (this.state.filters.indexOf(filter) > -1) {
      const index = this.state.filters.indexOf(filter);
      const filterDeleted = this.state.filters.splice(index, 1);
      this.setState({
        filters: filterDeleted
      });
      this.storeFilteredVideos(filterDeleted);
    } else {
      this.setState({
        filters: [filter]
      });
      this.storeFilteredVideos([filter]);
    }
  };

  render() {
    return (
      <div
        style={{
          marginLeft: "32px",
          marginRight: "32px",
          marginBottom: "90px"
        }}
      >
        <TagContainer width={this.props.width}>
          {this.getTags().map(e => {
            const name = e.name;
            const id = e.id;
            return (
              <TagButton
                selected={this.state.filters.indexOf(e.id) > -1}
                onClick={click => {
                  click.preventDefault();
                  this.changeFilters(e.id);
                  this.setState({ modalOpen: false });
                }}
                text={e.name}
              />
            );
          })}
        </TagContainer>
        <Stories
          videos={this.state.videoList.map((e, i) => e)}
          currentVideo={this.state.currentVideo}
          closeModal={() => this.setState({ modalOpen: false })}
          isOpen={this.state.modalOpen}
        />
        <Grid>
          {this.state.videoList.map((e, i) => {
            return (
              <ImageContainer key={i} width={this.props.windowSize.windowWidth}>
                <LazyLoad>
                  <Card
                    onClick={() =>
                      this.setState({
                        currentVideo: i,
                        modalOpen: true
                      })
                    }
                    e={e}
                    images={images}
                  />
                </LazyLoad>
              </ImageContainer>
            );
          })}
        </Grid>
      </div>
    );
  }
}

export default withWindowSizeListener(Filterlist);
