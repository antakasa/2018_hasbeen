import React, { Component } from "react";
import "./index.css";
import BurgerMenu from "react-burger-menu";
import classNames from "classnames";
import styled from "styled-components";
import { HashRouter as Router, Link } from "react-router-dom";

class MenuWrap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hidden: false
    };
  }

  componentWillReceiveProps(nextProps) {
    setTimeout(() => {
      this.show();
    }, this.props.wait);
  }

  show() {
    this.setState({ hidden: false });
  }

  render() {
    let style;

    if (this.state.hidden) {
      style = { display: "none" };
    }

    return (
      <div style={style} className={this.props.side}>
        {this.props.children}
      </div>
    );
  }
}

class Demo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentMenu: "stack",
      side: "top",
      isOpen: false
    };
  }

  close = () => {
    this.setState({ isOpen: false });
  };

  getMenu = () => {
    const Menu = BurgerMenu[this.state.currentMenu];
    const StyledLink = styled.span`
      font-family: Bungee;
      font-size: 40px;
      color: #df5d5d;
      margin-top: 1%;
      text-decoration: none;
      &:hover {
        color: white;
      }
    `;
    const style = {
      fontFamily: "Bungee",
      fontSize: "35px",
      color: "#df5d5d",
      textDecoration: "none"
    };

    const menuList = [
      { name: "HasBeen", route: "/" },
      { name: "Jaksot", route: "/jaksot" },
      { name: "Tarinat", route: "/tarinat" },
      { name: "Musiikki", route: "/musa" },
      { name: "Dallas Vlod", route: "/vlogit" }
    ];

    const items = [
      <Router>
        <React.Fragment>
          {menuList.map(e => (
            <Link to={e.route} style={{ textDecoration: "none" }}>
              <StyledLink
                key="0"
                href=""
                onClick={click => {
                  this.close();
                }}
              >
                {e.name}
              </StyledLink>
            </Link>
          ))}
        </React.Fragment>
      </Router>
    ];
    let jsx;
    jsx = (
      <MenuWrap wait={20} side={this.state.side}>
        <Menu
          width={"100vh"}
          id={this.state.currentMenu}
          pageWrapId={"page-wrap"}
          outerContainerId={"outer-container"}
          isOpen={this.state.isOpen}
          top
        >
          {items}
        </Menu>
      </MenuWrap>
    );
    return jsx;
  };

  render() {
    return <div>{this.getMenu()}</div>;
  }
}

export default Demo;
