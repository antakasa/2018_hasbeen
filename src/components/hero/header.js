import React from "react";
import { withWindowSizeListener } from "react-window-size-listener";
import GlitchText from "../GlitchText";
import skull from "./skull.svg";
import cloud from "./storm_cloud.svg";
import microphone from "./microphone.svg";
import { leftMarginCalculator } from "../../helpers/index.js";
const Header = ({ header, description, windowSize }) => {
  return (
    <div
      style={{
        position: "relative",
        marginTop: "-10%",
        marginBottom: "10%",
        marginLeft: leftMarginCalculator(windowSize.windowWidth),
        zIndex: 3
      }}
    >
      <div
        style={{
          fontFamily: "Bungee",
          position: "relative",
          zIndex: 2,
          fontSize: "40px",
          lineHeight: "1",
          marginLeft: "0px", // oli 15px?
          color: "white",
          marginBottom: "15px"
        }}
      >
        <GlitchText text={header} images={[skull, cloud, microphone]} />
      </div>
      <div
        style={{
          fontFamily: "Open Sans",
          fontSize: windowSize.windowWidth < 600 ? "15px" : "20px",
          lineHeight: "24px",
          marginLeft: "0px", // Oli 15px?
          color: "white"
        }}
      >
        {description}
      </div>
    </div>
  );
};

export default withWindowSizeListener(Header);
