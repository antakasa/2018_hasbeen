import React from "react";
import LazyHero from "react-lazy-hero";
import MainIMG from "./etusivu.jpg";
import WebFont from "webfontloader";
import Logo from "../logo/index.js";
export default class HeroContainer extends React.Component {
  componentDidMount() {
    // This stuff is trailer related, not relevant anymore but leaving it here just in case
    //const video = document.getElementById("trailer");
    //const id = "1-4390554";
    //if (window.yleVisualisation && window.yleVisualisation.embedYlePlayer) {
    //window.yleVisualisation.embedYlePlayer(video, id, {});
    //} else {
    //}
  }

  printTrailer = () => {
    // again, trailer related

    return (
      <React.Fraction>
        <div
          style={{
            fontFamily: "Bungee",
            fontSize: "40px",
            lineHeight: "1",
            marginLeft: "0px", // oli 15px?
            color: "white"
          }}
        >
          Katso traileri
        </div>
        <div
          style={{
            marginTop: "5vh",
            marginBottom: "5vh"
          }}
          id="trailer"
        />
      </React.Fraction>
    );
  };

  render() {
    const gradientDiv = {
      background: "linear-gradient(to bottom, rgba(63, 65, 77, 0), #333F4B)",
      height: "20vh",
      position: "relative",
      width: "100%",
      color: "white",
      zIndex: "0",
      marginTop: "-20vh"
    };

    return (
      <div>
        <LazyHero
          isCentered={true}
          className={"pr"}
          minHeight={this.props.height ? this.props.height : "50vh"}
          parallaxOffset={"20"}
          imageSrc={this.props.heroIMG}
          opacity={0}
        >
          {this.props.children}
        </LazyHero>
        <div style={gradientDiv} />
      </div>
    );
  }
}
