import React from "react";
import styled from "styled-components";
import Salla from "../label/imgs/salla.png";
import Niklas from "../label/imgs/niklas.png";
import Data from "../../data.json";
import { profileData, checkWhoseStory } from "../../helpers/index.js";
const Card = ({ e, images, onClick }) => {
  const who = checkWhoseStory(e);
  return (
    <div
      onClick={() => onClick()}
      style={{ position: "relative", width: "100%" }}
    >
      <img
        key={e}
        id={e}
        style={{ width: "100%" }}
        src={images[e] ? images[e] : e}
      />
      <Gradient />
      <Circle element={e} who={who} />
    </div>
  );
};
const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  position: absolute;
  bottom: 10px;
  width: 100%;
`;

const Image = styled.img`
  left: -2px;
  width: 100%;
  margin-left: auto;
  margin-right: auto;
  border-radius: 58%;
  border: 2px solid gray;
  position: relative;
  top: -2px;
`;

const Nick = styled.div`
  font-family: "Open Sans";
  color: white;
  font-size: 0.6em;
`;

const ImageContainer = styled.div`
  border-radius: 49%;
  border: 3px solid white;
  width: 7vh;
  height: 7vh;
`;

const Gradient = styled.div`
  background: linear-gradient(to bottom, rgba(0, 0, 0, 0), #000000);
  height: 42%;
  position: absolute;
  width: 100%;
  color: white;
  zindex: 2;
  bottom: 0px;
`;

const Circle = ({ who, element }) => (
  <Container>
    <ImageContainer>
      {" "}
      <Image src={profileData().image(who)} />
    </ImageContainer>
    <Nick>{profileData().nick(who)}</Nick>
  </Container>
);

export default Card;
