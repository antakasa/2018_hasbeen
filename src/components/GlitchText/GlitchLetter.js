import React, { Component } from 'react';
import { range } from '../../helpers';

const State = {
    STILL: 0,
    IMG: 1,
    BOUNCING: 2,
};

function rand(range) {
    return Math.floor(Math.random() * (range.max - range.min + 1) + range.min);
}

const randItem = (array) => !array || array.length === 0 ? undefined : array[rand(range(0, array.length - 1))];

export default class GlitchLetter extends Component {
    static get defaultProps() {
        return {
            images: [],
            translatex: range(-5.0, 5.0),
            translatey: range(-5.0, 5.0),
            rotate: range(-10, 10),
            stillTime: range(500, 4000),
            imageTime: range(200, 400),
            bouncingTime: range(200, 1000),
            bouncingInterval: range(50, 150),
        }
    }

    constructor(props) {
        super(props);

        this.state = {
            state: State.STILL,
            x: 0,
            y: 0,
            rot: 0,
            image: null,
        };
    }

    componentDidMount() {
        this._startStill();
    }

    _startStill = () => {
        clearInterval(this.intervalId);
        this.setState((prev) => ({...prev, state: State.STILL}));
        this._randomTimeout(this._startDisplayImage, this.props.stillTime);
    }

    _startDisplayImage = () => {
        const { images } = this.props;
        if (!images) {
            this.setState(prev => ({...prev, state: State.BOUNCING}));
        }

        let image = `url("${randItem(images)}")`
        this.setState((prev) => ({ ...prev, ...this._randomTransform(), image, state: State.IMG }));

        this._randomTimeout(this._startBouncing, this.props.imageTime);
    }

    _startBouncing = () => {
        this.setState((prev) => ({ ...prev, img: null, state: State.BOUNCING }));

        this._randomTimeout(this._startStill, this.props.bouncingTime);
        this._randomInterval(this._bounce, this.props.bouncingInterval);
    }

    _bounce = () => {
        this.setState((prev) => ({
            ...prev,
            ...this._randomTransform(),
        }))
    }

    _randomTransform() {
        const { translatex, translatey, rotate } = this.props;

        return {
            x: rand(translatex),
            y: rand(translatey),
            rot: rand(rotate),
        };
    }

    render() {
        const { letter } = this.props;
        const { state, x, y, rot, image } = this.state;

        const transform = () => `translate3d(${x}px, ${y}px, 0px) rotate3d(1, 1, 1, ${rot}deg)`;

        switch (state) {
            case State.STILL:
                return <span>{letter}</span>;
            case State.IMG:
                const imageStyle = {
                    transform: transform(),
                    backgroundImage: image,
                    color: 'transparent',
                    backgroundRepeat: 'no-repeat',
                    backgroundPosition: '50% 50%',
                    backgroundSize: '95%',
                };

                return <span style={imageStyle}>{letter}</span>;
            case State.BOUNCING:
                const bouncingStyle = {
                    transform: transform(),
                };
                return <span style={bouncingStyle}>{letter}</span>;
            default:
                throw Error(`Invalid state "${state}"`)
        }
    }

    _randomTimeout(fn, delayRange) {
        clearTimeout(this.timeoutId);
        this.timeoutId = setTimeout(fn, rand(delayRange));
    }

    _randomInterval(fn, intervalRange) {
        clearInterval(this.intervalId);
        this.intervalId = setInterval(fn, rand(intervalRange));
    }

    componentWillUnmount() {
        clearInterval(this.intervalId);
        clearTimeout(this.timeoutId);
    }
}