import React, { Component } from 'react';
import GlitchLetter from './GlitchLetter';
import { range } from '../../helpers';

const map = (arrayLike, transform) => Array.prototype.map.call(arrayLike, transform);

export default class GlitchText extends Component {
    static get defaultProps() {
        return {
            translatex: range(-5.0, 5.0),
            translatey: range(-5.0, 5.0),
            rotate: range(-10, 10),
        }
    }

    glitchLetter = (char, i) => {
        const { images } = this.props;
        return <GlitchLetter
            key={i}
            letter={char}
            images={images} />
    }

    render() {
        const { text, images } = this.props;
        return (
            <div style={{ display: 'flex' }}>
                {map(text, this.glitchLetter)}
            </div>
        );
    }
}

