import React from "react";
import styled from "styled-components";

import { HashRouter as Router, Link } from "react-router-dom";

const Tag = styled.span`
  font-size: 15px;
  text-decoration: none;
`;

const WatchEpisode = styled.div`
  text-transform: uppercase;
  font-size: 14px;
  background-color: #df5d5d;
  border-radius: 3px;
  padding: 10px;
  width: 35%;
  text-align: center;
  margin-top: 29px;
  margin-bottom: 29px;
  color: white;
  font-family: "Open Sans";
  &:hover {
    background: white;
    color: #333f4b;
  }
`;

const ShowMore = styled.div`
  font-size: 14px;
  margin-left: auto;
  margin-right: auto;
  padding: 10px;
  width: 35%;
  text-align: center;
  margin-top: 29px;
  border: 1px solid white;
  margin-bottom: 29px;
  color: white;
  font-family: "Open Sans";
  &:hover {
    background: white;
    color: #333f4b;
  }
`;

const TagContent = styled.span`
	  background-color: ${props => (props.selected ? "#df5d5d" : "#606060")};
    border-radius: 25px;
    font-size: 13px;
  font-family: 'Open Sans', sans-serif;
  padding: 6px 15px;
  margin-bottom: 5px;
  margin-left: 5px;
  display: inline-block
  color: #FFF;
  transition: all 300ms ease-in-out;
`;

const TagButton = ({ selected, onClick, text }) => (
  <Tag onClick={onClick}>
    <TagContent selected={selected}> {text}</TagContent>
  </Tag>
);

const ShowMoreButton = ({ children, newPath, additionalStyles }) => {
  if (!additionalStyles) additionalStyles = {};
  return (
    <Router>
      <Link to={newPath}>
        <ShowMore style={additionalStyles}>{children}</ShowMore>
      </Link>
    </Router>
  );
};
const WatchEpisodeButton = ({ children, newPath }) => (
  <Router>
    <Link to={newPath}>
      <WatchEpisode> {children}</WatchEpisode>
    </Link>
  </Router>
);

export { TagButton, ShowMoreButton, WatchEpisodeButton };
