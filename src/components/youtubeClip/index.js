import React from "react";
import YouTube from "react-youtube";

export default class extends React.Component {
  render() {
    const opts = {
      width: "100%",
      playerVars: {
        // https://developers.google.com/youtube/player_parameters
      }
    };

    return (
      <YouTube videoId={this.props.id} opts={opts} onReady={this._onReady} />
    );
  }

  _onReady(event) {
    // access to player in all event handlers via event.target
    event.target.pauseVideo();
  }
}
