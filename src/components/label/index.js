import React from "react";
import styled from "styled-components";
import niklas from "./imgs/niklas.png";
import salla from "./imgs/salla.png";
import { profileData } from "../../helpers/index.js";

const Container = styled.div`
  display: flex;
  flex-direction: row;
  height: 45px;
  margin-left: 15px;
  margin-top: 47px;
  margin-bottom: 15px;
`;
const ImageHolder = styled.div``;

const Image = styled.img`
  max-height: 100%;
  width: 45px;
`;

const TextHolder = styled.div`
  display: flex;
  flex-direction: column;
  font-size: 14px;
  margin-left: 17px;
  margin-top: auto;
  margin-bottom: auto;
  color: white;
  font-family: "Open Sans";
`;

const Nick = styled.div`
  height: 50%;
  text-transform: uppercase;
`;

const Handle = styled.div`
  height: 50%;
  color: #df5d5d;
`;

const Label = ({ nick, handle }) => {
  return (
    <Container id="hb_label_cont">
      <ImageHolder id="hb_image_holder">
        <Image src={profileData().image(nick)} />
      </ImageHolder>
      <TextHolder id="hb_text_holder">
        <Nick id="hb_nick">{nick}</Nick>
        <Handle id="hb_handle">@{handle}</Handle>
      </TextHolder>
    </Container>
  );
};

export default Label;
