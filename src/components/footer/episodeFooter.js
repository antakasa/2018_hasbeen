import React from "react";
import EpisodePreview from "../../components/episodePreview/index.js";
import { withWindowSizeListener } from "react-window-size-listener";
import ContentContainer from "../../components/ContentContainer";
import LogoFooter from "./index.js";
import SubHeader from "../subHeader/index.js";
import { ShowMoreButton } from "../../components/buttons";

//KORJAA: NUOLET VÄÄRIN
//<EpisodePreview width={windowSize.windowWidth} />
const Footer = ({ windowSize, children }) => (
  <div>
    <div
      style={{
        background: "#323441",
        textAlign: "center",
        width: "100vw",
        display: "flex",
        alignItems: "center",
        justifyContent: "center"
      }}
    >
      <ContentContainer style={{ paddingTop: "10%" }}>
        <ShowMoreButton newPath="/jaksot">Näytä kaikki jaksot</ShowMoreButton>
      </ContentContainer>
    </div>

    <LogoFooter />
  </div>
);

export default withWindowSizeListener(Footer);
