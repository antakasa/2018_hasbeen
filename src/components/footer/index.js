import React from "react";
import logo from "./hb_logo.svg";

const Footer = ({ children }) => (
  <div
    style={{
      background: "#323441",
      height: "80px",
      textAlign: "center",
      display: "grid",
      alignItems: "center",
      justifyContent: "center"
    }}
  >
    <img src={logo} style={{ height: "40%" }} />
  </div>
);

export default Footer;
