import React from "react";
import Swipe from "../swipe.js";
import { episodeImages } from "../../helpers/index.js";
import styled from "styled-components";
import Data from "../../textContent.js";
import { withRouter, HashRouter as Router, Link } from "react-router-dom";

const EpisodeDetailContainer = styled.div`
  display: flex;
  flex-direction: column;
  font-family: "Open Sans", sans-serif;
  position: absolute;
  justify-content: center;
  align-items: center;
  color: white;
  width: 100%;
  height: 100%;
`;

const EpisodeNumber = styled.div`
  font-size: ${props => (props.width < 600 ? "10px" : "14px")};
`;
const EpisodeName = styled.div`
  font-size: ${props => (props.width < 600 ? "15px" : "24px")}
  text-transform: uppercase;
  color: ${props => (props.comingSoon ? "#df5d5d" : "#fff")}
`;

const Coming = styled.div`
  font-size: ${props => (props.width < 600 ? "10px" : "14px")};
  color: #fff;
  text-transform: uppercase;
`;

const EpisodePreview = ({ src, width, location, i }) => (
  <Router>
    <Swipe>
      {episodeImages().map((e, i) => {
        return (
          <Link
            key={i}
            to={Data.jaksot[i].tulossa ? location.pathname : `/jaksot/${i + 1}`}
          >
            <EpisodeDetailContainer>
              {Data.jaksot[i].tulossa && (
                <Coming width={width}>Tulossa {Data.jaksot[i].tulossa}</Coming>
              )}
              <EpisodeNumber width={width}>Jakso {i + 1}</EpisodeNumber>
              <EpisodeName width={width}>
                {Data.jaksot[i].jaksonNimi}
              </EpisodeName>
            </EpisodeDetailContainer>

            <img
              alt="img"
              style={{ width: "100%" }}
              data-src={e}
              className="swiper-lazy"
            />
            <div className="swiper-lazy-preloader swiper-lazy-preloader-white" />
          </Link>
        );
      })}
    </Swipe>
  </Router>
);

export default withRouter(EpisodePreview);
