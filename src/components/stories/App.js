import React from "react";
import CSSTransition from "react-transition-group/CSSTransition";
import "./App.css";
import styled from "styled-components";
import { ProgressBar, MuteToggleButton } from "react-player-controls";
import Video from "./video.js";
import shouldIGbeDisplayed from "./igCards.js";
import {
  Wrapper,
  Head,
  ModalContainer,
  ModalInner,
  ModalBody,
  CloseButton,
  ImageContainer,
  Image,
  HeadContainer,
  ProgerssBarContainer
} from "./videoElements.js";
import { checkWhoseStory } from "../../helpers/index.js";
import Analytics from "../../helpers/analytics.js";
import registerStoryRound from "./addOneRound.js";
export default class ModalVideo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      totalTime: 5,
      currentTime: 0,
      duration: 0,
      muted: true,
      videos: this.props.videos,
      currentVideo:
        typeof this.props.currentVideo === "number"
          ? this.props.currentVideo
          : 0
    };
  }

  openModal = () => {
    this.setState({ isOpen: true });
  };

  closeModal = () => {
    this.setState({ isOpen: false });
    if (this.props.toggleAnimation) this.props.toggleAnimation();
    this.props.closeModal();
    if (typeof this.props.onClose === "function") {
      this.props.onClose();
    }
  };

  componentWillReceiveProps(nextProps) {
    if (!window.HBAnalytics) window.HBAnalytics = Analytics;

    this.setState({
      isOpen: nextProps.isOpen,
      videos: nextProps.videos,
      currentVideo: nextProps.currentVideo
    });
  }

  updateTime = (current, duration) => {
    this.setState({ currentTime: current, duration: duration });
  };

  changeSource = () => {
    const newRound = registerStoryRound();
    Analytics.trackStoryMilestone(newRound);
    let newVideo;
    if (shouldIGbeDisplayed(newRound)) {
      newVideo = shouldIGbeDisplayed(newRound);
    } else {
      const currentVideo = this.state.currentVideo;
      newVideo = currentVideo + 1;
      if (this.state.videos.length <= currentVideo + 1) {
        this.closeModal();
        this.setState({ currentVideo: 0 });
        return;
      }
      this.setState({
        currentVideo: newVideo
      });
    }

    const newSource = this.getPathToVideo(newVideo);

    if (this.state.player && this.state.player.play) {
      this.state.player.src = newSource;
      this.state.player.load();
      const playPromise = this.state.player.play();

      if (playPromise !== undefined) {
        playPromise.then(_ => {}).catch(error => {});
      }
    }
  };

  getPathToVideo = newVid => {
    if (typeof newVid === "number") {
      const name = this.state.videos[newVid];
      return `https://yleerilliset-a.akamaihd.net/YleAihe/2018/02_hasbeen/290318/${name}.mp4`;
    } else {
      return newVid;
    }
  };

  setListeners = ref => {
    ref.onended = () => {
      this.changeSource();
    };
    ref.ontimeupdate = () => this.updateTime(ref.currentTime, ref.duration);
  };

  storeRef = ref => {
    this.setState({ player: ref });
    this.setListeners(ref);
  };

  componentDidUpdate() {
    if (this.state.isOpen && this.modal) {
      this.modal.focus();
    }
  }

  render() {
    return (
      <div>
        <CSSTransition>
          {() => {
            if (!this.state.isOpen) {
              return null;
            }
            return (
              <ModalContainer
                tabIndex="-1"
                role="dialog"
                innerRef={node => {
                  this.modal = node;
                }}
              >
                <ModalBody>
                  <ModalInner>
                    <Wrapper>
                      <Head
                        whoIsIt={checkWhoseStory(
                          this.state.videos[this.state.currentVideo]
                        )}
                      >
                        <ProgressBar
                          totalTime={this.state.duration}
                          width={"80%"}
                          currentTime={this.state.currentTime}
                          isSeekable={false}
                        />
                        <CloseButton
                          ref={node => {
                            this.modalbtn = node;
                          }}
                          onClick={() => {
                            this.closeModal();
                          }}
                        />

                        <MuteToggleButton
                          isMuted={this.state.muted}
                          onMuteChange={() => {
                            this.state.player.muted = !this.state.player.muted;
                            this.setState({ muted: this.state.player.muted });
                          }}
                        />
                        <div style={{ textAlign: "center", color: "white" }}>
                          {this.state.videos[this.state.currentVideo]}
                        </div>
                      </Head>
                      {this.state.videos[this.state.currentVideo] ? (
                        <Video
                          updateTime={this.updateTime}
                          changeMuteOnUnmount={() =>
                            this.setState({ muted: true })
                          }
                          storeRef={this.storeRef}
                          src={`https://yleerilliset-a.akamaihd.net/YleAihe/2018/02_hasbeen/290318/${
                            this.state.videos[this.state.currentVideo]
                          }.mp4`}
                          changeSource={this.changeSource}
                        />
                      ) : (
                        <div style={{ color: "white", textAlign: "center" }}>
                          VIDEOTA EI LÖYDY
                        </div>
                      )}
                    </Wrapper>
                  </ModalInner>
                </ModalBody>
              </ModalContainer>
            );
          }}
        </CSSTransition>
      </div>
    );
  }
}
