import React from "react";
import styled from "styled-components";
import { profileData } from "../../helpers/index.js";

const ImageContainer = styled.div`
  display: flex;
  align-items: center;
  color: white;
  height: 45px;
  font-family: "Open Sans";
  width: 50%;
`;

const CloseButtonContainer = styled.div`
  display: flex;
  justify-content: center;
  align-self: center;
  justify-content: flex-end;
  width: 15%;
`;

const Image = styled.img`
  width: 45px;
  height: 100%;
`;

const HeadContainer = styled.div`
  display: flex;
  flex-direction: column
  width: 100%;
  position: absolute;
  top: 0px;
}
`;

const ProgressBarContainer = styled.div`
  width: 100%;
`;

const SecondRowContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
`;

const Nick = styled.div`
  margin-left: 10px;
`;

const Wrapper = styled.div`
  @keyframes modal-video-inner {
    from {
      transform: translate(0, 100px);
    }
    to {
      transform: translate(0, 0);
    }
  }
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  padding-bottom: 56.25%;
  background-color: #000;
  animation-timing-function: ease-out;
  animation-duration: 0.3s;
  animation-name: modal-video-inner;
  transform: translate(0, 0);
  transition: transform 0.3s ease-out;
  user-select: none;
  -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
  cursor: default;
`;

const CloseButton = styled.div`
  position: relative;
  z-index: 2;
  align-self: end;
  justify-self: end;
  display: inline-block;
  width: 35px;
  height: 35px;
  overflow: hidden;
  border: none;
  background: transparent;
  &:before {
    transform: rotate(45deg);
    content: "";
    position: absolute;
    height: 2px;
    width: 100%;
    top: 50%;
    left: 0;
    margin-top: -1px;
    background: #fff;
    border-radius: 5px;
    margin-top: -6px;
  }

  &:after {
    transform: rotate(-45deg);
    content: "";
    position: absolute;
    height: 2px;
    width: 100%;
    top: 50%;
    left: 0;
    margin-top: -1px;
    background: #fff;
    border-radius: 5px;
    margin-top: -6px;
  }
`;

const ModalInner = styled.div`
  display: table-cell;
  vertical-align: middle;
  width: 100%;
  height: 100%;
  user-select: none;
  -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
  cursor: default;
`;

const ModalBody = styled.div`
  max-width: 100vw;

  width: 100%;
  height: 100%;
  margin: 0 auto;
  display: table;
`;

const ModalContainer = styled.div`
  @keyframes modal-video {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }

  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 1000000;
  cursor: pointer;
  opacity: 1;
  animation-timing-function: ease-out;
  animation-duration: 0.3s;
  animation-name: modal-video;
  transition: opacity 0.8s ease-out;

  user-select: none;
  -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
  cursor: default;
`;

const Head = props => (
  <HeadContainer id="headContainer">
    <ProgressBarContainer id="PBContainer">
      {" "}
      {props.children[0]}
    </ProgressBarContainer>
    <SecondRowContainer id="secondRowContainer">
      <ImageContainer id="imageContainer">
        <Image src={profileData().image(props.whoIsIt)} />
        <Nick>
          <strong>{profileData().nick(props.whoIsIt)}</strong>
        </Nick>
      </ImageContainer>
      {props.children[2]}
      <CloseButtonContainer>{props.children[1]}</CloseButtonContainer>
    </SecondRowContainer>
  </HeadContainer>
);

export {
  Wrapper,
  ModalContainer,
  ModalInner,
  ModalBody,
  CloseButton,
  ImageContainer,
  Image,
  HeadContainer,
  ProgressBarContainer,
  Head
};
