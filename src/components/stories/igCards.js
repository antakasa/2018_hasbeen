import { getRandomElementsFromArray, importAll } from "../../helpers/index.js";

const divideToArrays = (object, who, array) => {
  for (var key in object) {
    if (object.hasOwnProperty(key) && key.includes(who)) {
      array.push(object[key]);
    }
  }
  return array;
};

const giveRandomIGCard = (() => {
  const images = importAll(
    require.context("../../imgs/followIGCard", false, /\.(png|mp4|svg)$/)
  );

  let salla = divideToArrays(images, "salla", []);
  let niklas = divideToArrays(images, "niklas", []);
  return {
    salla: () => getRandomElementsFromArray(salla)[0],
    niklas: () => getRandomElementsFromArray(niklas)[0]
  };
})();

const shouldIGbeDisplayed = newRound => {
  if ([10, 30, 60].includes(newRound)) return giveRandomIGCard.salla();
  if ([3, 20, 40].includes(newRound)) return giveRandomIGCard.niklas();
  return null;
};

export default shouldIGbeDisplayed;
