import { sessionGetSets } from "../../helpers/index.js";

const registerStoryRound = () => {
  const currentRound = sessionGetSets.get();
  sessionGetSets.set(currentRound + 1);
  return currentRound + 1;
};

export default registerStoryRound;
