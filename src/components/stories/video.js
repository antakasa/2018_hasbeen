import React from "react";
import styled from "styled-components";
import Analytics from "../../helpers/analytics.js";

const VideoEl = styled.video`
  user-select: none;
  height: 100%;
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
  cursor: default;
`;

export default class Video extends React.Component {
  componentDidMount() {
    if (this.player) {
      this.props.storeRef(this.player);
      this.player.play();
    }
    Analytics.registerStoryStart();
  }

  componentWillUnmount() {
    this.props.changeMuteOnUnmount();
  }

  render() {
    return (
      <VideoEl
        src={this.props.src}
        onClick={() => this.props.changeSource()}
        innerRef={comp => (this.player = comp)}
        playsInline
        autoPlay
        muted
      />
    );
  }
}
