import React from "react";
import Grid from "../../components/grid/index.js";
import styled from "styled-components";
import SpotifyPlayer from "react-spotify-player";
import { withWindowSizeListener } from "react-window-size-listener";
import { createRandomArray } from "../../helpers/index.js";

const margin = 20;

const calculateHeight = fullScreenWidth => {
  if (fullScreenWidth < 600) return 80;
  let contentWidth = fullScreenWidth / 2;
  let margin = 20;
  let playerWidth = contentWidth / 2 - margin;
  let playerHeight = playerWidth + 0.22 * playerWidth;
  return playerHeight;
};

const calculateWidth = fullScreenWidth => {
  if (fullScreenWidth < 600) return fullScreenWidth - 30;
  let contentWidth = fullScreenWidth / 2;
  let margin = 20;
  let playerWidth = contentWidth / 2 - margin;
  return playerWidth;
};

const GridContainer = styled.div`
  margin-bottom: 70px;
  margin-left: auto;
  margin-right: auto;
`;

const Container = styled.div`
  margin-left: 15px;
  margin-bottom: ${margin}px;
  height: ${props => (props.width < 1000 ? "80px" : "")};
`;

const GridElement = ({ width, src }) => {
  return (
    <Container width={width}>
      <SpotifyPlayer
        uri={src}
        size={{ width: calculateWidth(width), height: calculateHeight(width) }}
        view={"coverart"}
        theme={"black"}
      />
    </Container>
  );
};

class Music extends React.Component {
  componentDidUpdate() {}
  render() {
    const { data } = this.props;
    return (
      <GridContainer id="spotify_container">
        <Grid>
          {data.map(e => {
            if (e.spotifyURI.length < 2) return;
            return (
              <GridElement
                src={e.spotifyURI}
                width={this.props.windowSize.windowWidth}
              />
            );
          })}
        </Grid>
      </GridContainer>
    );
  }
}
export default withWindowSizeListener(Music);
