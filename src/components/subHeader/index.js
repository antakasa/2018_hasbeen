import React from "react";
import styled from "styled-components";
import { HashRouter as Router, Link } from "react-router-dom";
const Header = styled.div`
  font-family: "Bungee";
  font-size: 29px;
  color: white;
  line-height: 1;
  margin-left: 15px;
  margin-bottom: 47px;
`;

const SubHeader = ({ children, link, additionalStyles }) => {
  if (!additionalStyles) additionalStyles = {};
  if (!link) return <Header style={additionalStyles}>{children}</Header>;
  return (
    <Link to={link} style={{ textDecoration: "none" }}>
      <Header style={additionalStyles}>{children}</Header>
    </Link>
  );
};

export default SubHeader;
