import React from "react";
import logo from "./hb_logo.svg";
import { HashRouter as Router, Link } from "react-router-dom";
import { withWindowSizeListener } from "react-window-size-listener";
const Logo = ({ windowSize, center, topLeft }) => {
  if (center) {
    return (
      <img
        style={{
          width: windowSize.windowWidth < 600 ? "100%" : "",
          height: windowSize.windowWidth < 600 ? "36px" : "25vh"
        }}
        src={logo}
      />
    );
  } else if (topLeft) {
    return (
      <Router>
        <Link to="/">
          <img
            style={{
              position: "absolute",
              top: "15px",
              left: "15px",
              height: "36px"
            }}
            src={logo}
          />
        </Link>
      </Router>
    );
  }
};

export default withWindowSizeListener(Logo);
