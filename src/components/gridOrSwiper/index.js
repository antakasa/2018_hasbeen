import React, { Component } from "react";
import Swiper from "react-id-swiper";
import "../../../node_modules/swiper/dist/css/swiper.css";
import { withWindowSizeListener } from "react-window-size-listener";
import Grid from "../grid/index.js";
import styled from "styled-components";
import { filter } from "../../helpers/index.js";
import LazyLoad from "react-lazy-load";
import Stories from "../stories/App.js";
import {
  timeLimitStore,
  sortToAscendingOrder,
  filterBasedOnEpisodeNumber
} from "../../helpers/index.js";
const GridItem = styled.div`
  flex-grow: 1;
  box-sizing: border-box;
  color: #171e42;
  margin-left: 40px;
  margin-top: 10px;
  max-width: calc(33.33333% - 40px);
`;

class Episodes extends Component {
  state = {};

  componentWillMount() {
    const { filters } = this.props;
    let filtered = [];
    if (typeof filters === "number") {
      filtered = filterBasedOnEpisodeNumber(filters).sort(sortToAscendingOrder);
    } else {
      filtered = filter(filters, null, timeLimitStore()).sort(
        sortToAscendingOrder
      );
    }
    this.setState({ videoList: filtered, currentVideo: 0 });
  }

  changeCurrentVideo = currentVideo => {
    this.setState({
      currentVideo: currentVideo,
      modalOpen: true
    });
  };

  render() {
    const { modalOpen, videoList, currentVideo } = this.state;

    const params = {
      loop: true,
      lazy: {
        loadPrevNext: true,
        loadPrevNextAmount: 2
      },
      spaceBetween: 10,
      slidesPerView: 2
    };

    return (
      <div>
        <div>
          <Stories
            videos={videoList.map((e, i) => e.tiedostonNimi)}
            currentVideo={currentVideo}
            isOpen={modalOpen}
            toggleAnimation={this.props.toggleAnimation}
            closeModal={() => this.setState({ modalOpen: false })}
          />
          {this.props.windowSize.windowWidth < 420 && videoList.length > 2 ? (
            <Swiper {...params} style={{ marginLeft: "15px" }}>
              {videoList.map((e, i) => (
                <div>
                  {React.cloneElement(this.props.children, {
                    e: e.tiedostonNimi,
                    onClick: () => {
                      if (this.props.toggleAnimation) {
                        this.props.toggleAnimation();
                      }
                      this.changeCurrentVideo(i);
                    }
                  })}
                </div>
              ))}
            </Swiper>
          ) : (
            <Grid>
              {videoList.map((e, i) => {
                if (i >= this.props.numberOfItemsInGrid) return;
                return (
                  <GridItem>
                    <LazyLoad>
                      {React.cloneElement(this.props.children, {
                        e: e.tiedostonNimi,
                        onClick: () => {
                          if (this.props.toggleAnimation) {
                            this.props.toggleAnimation();
                          }
                          this.changeCurrentVideo(i);
                        }
                      })}
                    </LazyLoad>
                  </GridItem>
                );
              })}
            </Grid>
          )}
        </div>
      </div>
    );
  }
}

export default withWindowSizeListener(Episodes);
