import React from "react";
import { withWindowSizeListener } from "react-window-size-listener";
import styled from "styled-components";

const calculateWidth = width => {
  if (width > 500 && width < 860) {
    return "80%";
  } else if (width < 500) {
    return "100%";
  } else {
    return "50%";
  }
};

const Content = styled.div`
  width: ${props => calculateWidth(props.width)}
  margin-left: auto;
  margin-right: auto;
`;

const ContentContainer = ({ windowSize, children }) => {
  if (windowSize.windowWidth === null) return null;
  return <Content width={windowSize.windowWidth}>{children}</Content>;
};

export default withWindowSizeListener(ContentContainer);
