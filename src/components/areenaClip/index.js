import React from "react";
class AreenaClip extends React.Component {
  componentDidMount() {
    const { ylePlayer } = window;
    const element = this.player;
    const { id } = this.props;

    const onPlayerReady = () => {
      console.log("playerReady");
    };

    if (ylePlayer && ylePlayer.render) {
      const player = ylePlayer.render({
        element,
        props: {
          id,
          playFullScreen: true
        },
        onPlayerReady: onPlayerReady
      });
    }
  }

  render() {
    return (
      <div
        style={{ height: "100%" }}
        id="videoPlayer"
        ref={component => (this.player = component)}
      />
    );
  }
}

export default AreenaClip;
