import React from "react";
import styled, { keyframes } from "styled-components";
import { withWindowSizeListener } from "react-window-size-listener";
const keyFrame = keyframes`
  0% {
      opacity: 0;
  }
  100% {
  	opacity: 1
  }
`;

const FlexboxGrid = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-left: 0px;
  margin-top: 0px;
  animation-name: ${keyFrame};
  animation-duration: 0.4s;
  animation-timing-function: ease;
  animation-delay: 0s;
  animation-iteration-count: 1;
  animation-direction: normal;
  animation-fill-mode: forwards;
  animation-play-state: running;
`;

const Grid = ({ children }) => {
  return <FlexboxGrid id="grid">{children}</FlexboxGrid>;
};

export default withWindowSizeListener(Grid);
