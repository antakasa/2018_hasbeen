import React, { Component } from "react";
import Swiper from "react-id-swiper";
import "../../node_modules/swiper/dist/css/swiper.css";
import { ResizeSensor } from "../helpers/index.js";
import styled from "styled-components";
import { withWindowSizeListener } from "react-window-size-listener";
import leArrImg from "../imgs/arrow_left.svg";
import riArrImg from "../imgs/arrow_right.svg";
const LeftArrow = styled.img`
  position: relative;
  cursor: pointer;
  left: 0;
  z-index: 2;
  bottom: ${props =>
    props.contHeight ? props.contHeight / 2 + "px" : "150px"};
  height: 35px;
`;
const RightArrow = styled.img`
  position: relative;
  cursor: pointer;
  left: calc(100% - 53px);
  bottom: ${props =>
    props.contHeight ? props.contHeight / 2 + "px" : "150px"};
  z-index: 2;
  height: 35px;
`;

const resizeListener = (container, callback) => {};

class SwiperApp extends Component {
  state = { height: "300" };

  componentDidUpdate() {}

  componentDidMount() {
    this.resizeListener(this.container, this.changeHeight);
  }

  resizeListener = (container, changeHeight) => {
    new ResizeSensor(container, function() {
      changeHeight(container.clientHeight);
    });
  };
  goNext = () => {
    if (this.swiper) this.swiper.slideNext();
  };

  goPrev = () => {
    if (this.swiper) this.swiper.slidePrev();
  };

  changeHeight = height => {
    this.setState({ height: height });
  };

  render() {
    let thisbind = this;
    const params = {
      loop: true,
      spaceBetween: 40,
      // Disable preloading of all images
      preloadImages: true,
      updateOnImagesReady: true,
      // Enable lazy loading
      lazy: true,
      on: {
        lazyImageReady: function() {
          thisbind.changeHeight(thisbind.container.clientHeight);
        }
      },
      lazy: {
        loadPrevNext: true,
        loadPrevNextAmount: 2
      },
      slidesPerView: 2,
      breakpoints: {
        700: {
          slidesPerView: 1.5,
          spaceBetween: 10
        }
      }
    };
    return (
      <div ref={cont => (this.container = cont)}>
        <Swiper
          {...params}
          ref={node => {
            node ? (this.swiper = node.swiper) : (this.swiper = null);
          }}
        >
          {this.props.children}
        </Swiper>
        <div id="arrowCont">
          {this.props.windowSize.windowWidth > 800 && (
            <React.Fragment>
              <LeftArrow
                contHeight={this.state.height}
                src={leArrImg}
                id="left"
                onClick={this.goPrev}
              />
              <RightArrow
                src={riArrImg}
                contHeight={this.state.height}
                id="right"
                onClick={this.goNext}
              />
            </React.Fragment>
          )}
        </div>
      </div>
    );
  }
}

export default withWindowSizeListener(SwiperApp);
