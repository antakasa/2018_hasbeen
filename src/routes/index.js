import React from "react";
import { HashRouter as Router, Switch, Route } from "react-router-dom";
import FrontPage from "../pages/frontpage/index.js";
import TextContent from "../textContent.js";
import StoriesPage from "../pages/stories/index.js";
import MusicPage from "../pages/music/index.js";
import EpisodePage from "../pages/episodes/index.js";
import EpisodeRoutes from "./episode/index.js";
import AreenaRoutes from "./areena/index.js";
import ScrollToTop from "./scrollToTop.js";
import Analytics from "../helpers/analytics.js";
import VlogRoutes from "./vlog/index.js";
const MainRoutes = () => {
  return (
    <Router>
      <ScrollToTop>
        <Switch>
          <Route exact path="/" render={() => <FrontPage />} />
          <Route path="/vlogit" component={VlogRoutes} />
          <Route path="/jaksot" component={EpisodeRoutes} />

          <Route
            exact
            path="/tarinat"
            render={() => {
              Analytics.registerEvent("pages.tarinat");
              return (
                <StoriesPage
                  kicker={TextContent.sivut.tarinat}
                  changeSite={this.changeSite}
                />
              );
            }}
          />

          <Route path="/areena" component={AreenaRoutes} />
          <Route
            exact
            path="/musa"
            render={() => {
              Analytics.registerEvent("pages.musiikki");
              return (
                <MusicPage
                  kicker={TextContent.sivut.musiikki}
                  data={TextContent.musiikki}
                  changeSite={this.changeSite}
                />
              );
            }}
          />
          <Route render={() => <FrontPage />} />
        </Switch>
      </ScrollToTop>
    </Router>
  );
};

export default MainRoutes;
