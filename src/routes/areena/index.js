import React from "react";
import AreenaClip from "../../components/areenaClip/index.js";
import { HashRouter as Router, Switch, Route } from "react-router-dom";
import Data from "../../textContent.js";
import Analytics from "../../helpers/analytics.js";

const DetermineId = epNum => Data.jaksot[epNum - 1].areenaID;

const PlayEpisode = ({ match }) => {
  Analytics.registerEvent(`areenaplayer.jakso.${match.params.episodeId}`);
  return (
    <div style={{ height: "100vh" }}>
      <AreenaClip id={DetermineId(match.params.episodeId)} />
    </div>
  );
};

const AreenaRoutes = ({ match }) => {
  return (
    <Router>
      <Route path={`/areena/:episodeId`} component={PlayEpisode} />
    </Router>
  );
};

export default AreenaRoutes;
