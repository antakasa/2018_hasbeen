import React from "react";
import { HashRouter as Router, Switch, Route } from "react-router-dom";
import EpisodePage from "../../pages/vlog/index.js";
import TextContent from "../../textContent.js";
import IndividualVlog from "../../pages/individualVlog/index.js";
import Analytics from "../../helpers/analytics.js";

const EpisodeRoutes = ({ match }) => (
  <Switch>
    <Route
      exact
      path={`${match.path}`}
      render={() => {
        Analytics.registerEvent(`pages.vlog`);
        return (
          <EpisodePage
            data={TextContent.vlogit}
            kicker={TextContent.sivut.vlog}
            changeSite={() => null}
          />
        );
      }}
    />

    {TextContent.vlogit.map((e, i) => {
      return (
        <Route
          path={`${match.path}/${i + 1}`}
          render={({ match }) => {
            Analytics.registerEvent(`pages.vlog.${i + 1}`);
            return (
              <IndividualVlog
                data={TextContent.vlogit[i]}
                episodeNumber={i + 1}
              />
            );
          }}
        />
      );
    })}

    <Route
      render={() => (
        <EpisodePage
          data={TextContent.vlogit}
          kicker={TextContent.sivut.vlog}
          changeSite={() => null}
        />
      )}
    />
  </Switch>
);
export default EpisodeRoutes;
