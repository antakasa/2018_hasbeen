import React from "react";
import { HashRouter as Router, Switch, Route } from "react-router-dom";
import EpisodePage from "../../pages/episodes/index.js";
import TextContent from "../../textContent.js";
import IndividualEpisode from "../../pages/individualEpisode/index.js";
import Analytics from "../../helpers/analytics.js";

const EpisodeRoutes = ({ match }) => (
  <Switch>
    <Route
      exact
      path={`${match.path}`}
      render={() => {
        Analytics.registerEvent(`pages.jaksot`);
        return (
          <EpisodePage
            data={TextContent.jaksot}
            kicker={TextContent.sivut.jaksot}
            changeSite={() => null}
          />
        );
      }}
    />

    {TextContent.jaksot.map((e, i) => {
      return (
        <Route
          path={`${match.path}/${i + 1}`}
          render={({ match }) => {
            Analytics.registerEvent(`pages.jakso.${i + 1}`);
            return (
              <IndividualEpisode
                data={TextContent.jaksot[i]}
                episodeNumber={i + 1}
              />
            );
          }}
        />
      );
    })}

    <Route
      render={() => (
        <EpisodePage
          data={TextContent.jaksot}
          kicker={TextContent.sivut.jaksot}
          changeSite={() => null}
        />
      )}
    />
  </Switch>
);
export default EpisodeRoutes;
