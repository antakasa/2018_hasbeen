import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import * as load from "load-script";
import { loadFont } from "./helpers/index.js";
import Menu from "./components/menu/index.js";
import MainRoutes from "./routes/index.js";
import { extendStringObject } from "./helpers/index.js";
class HasBeenApp extends React.Component {
  constructor(args) {
    super(...args);
    this.state = {
      modal: false,
      slideNumber: 1,
      currentSite: "Route"
    };
    extendStringObject();
  }

  changeSite = site => this.setState({ currentSite: site });

  render() {
    return (
      <React.Fragment>
        <Menu changeSite={this.changeSite} />
        <MainRoutes />
      </React.Fragment>
    );
  }
}

document.addEventListener("DOMContentLoaded", function(event) {
  load("https://player-v2.yle.fi/embed.js", function(err, script) {
    if (err) {
      console.log(err);
    } else {
      loadFont(["Bungee", "Open Sans"], () =>
        ReactDOM.render(<HasBeenApp />, document.getElementById("root"))
      );
    }
  });
});
